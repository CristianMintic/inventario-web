from rest_framework import views
from rest_framework.response import Response
from inventario.models.inventario import Inventario
from inventario.serializers.inventario_serializers import MostrarInventarioSerializer, CrearInventarioSerializer



# Aca se crean los EndPoints


class InventarioView(views.APIView):       
    
    def get(self, request):                 #visualizacion de todos los dispositivos en inventario
        lista_inventario = Inventario.objects.all()
        serializer = MostrarInventarioSerializer(lista_inventario, many=True)
        return Response(serializer.data, 200)

    def post(self, request):                #ingresa un dispositivo  a inventario
        datos_json = request.data           #Datos en Json
        serializer = CrearInventarioSerializer(data=datos_json)  #des_serializado de Datos en Json
        if serializer.is_valid():
            serializer.save()               #si es valido los graba en la base de datos
            return Response({"mensaje": "Ingresado a Inventario"}, 201)
        else:
            return Response(serializer.errors, 405)

           