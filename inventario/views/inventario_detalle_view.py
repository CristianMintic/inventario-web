from rest_framework import views
from rest_framework.response import Response
from inventario.models.inventario import Inventario
from inventario.serializers.inventario_serializers import MostrarInventarioSerializer, CrearInventarioSerializer, SacarInventarioSerializer


class InventarioDetallesView(views.APIView):
    def get(self, request, pk):                           # Busca un dispositivo filtrado por el serial
        try:
            dispositivo = Inventario.objects.get(serial=pk)
            serializer = MostrarInventarioSerializer(dispositivo)
            return Response(serializer.data, 200)
        except:
            return Response({"mensaje": "dispositivo no existente"}, 400)

    def put(self, request, pk):                            # Actualizo (saco) un dispositivo
        try:
            dispositivo = Inventario.objects.get(serial=pk)
            serializer = SacarInventarioSerializer(dispositivo,data=request.data)
            if  serializer.is_valid():
                serializer.save()
                return Response(serializer.data, 200)
            else:
                return Response(serializer.errors, 400)
        except:
            return Response({"mensaje": "dispositivo no existente"}, 400)

    def delete(self, request, pk):                          # Borra un dispositivo
        try:
            dispositivo = Inventario.objects.get(serial=pk)
            dispositivo.delete()
            return Response({"mensaje": "dispositivo borrado"}, 200)
        except:
            return Response({"mensaje": "dispositivo no existente"}, 400)