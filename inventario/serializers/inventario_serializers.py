from rest_framework import serializers
from  inventario.models.inventario import Inventario  # importo la clase 



class CrearInventarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventario
        fields = ['nombre_ingreso','area','ciudad','producto','cantidad','serial','descripcion','comentario_ingreso','fecha_ingreso']


class SacarInventarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventario
        fields = ['nombre_salida','comentario_salida','fecha_salida']


class MostrarInventarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventario
        fields = ["id",'nombre_ingreso','nombre_salida','area','ciudad','producto','cantidad','serial','descripcion','comentario_ingreso','fecha_ingreso','comentario_salida','fecha_salida']

   