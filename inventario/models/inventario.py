from django.db import models

class Inventario(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre_ingreso = models.CharField(max_length=30)
    nombre_salida = models.CharField(max_length=30)
    area = models.CharField(max_length=25)
    ciudad = models.CharField(max_length=25)
    producto = models.CharField(max_length=15)
    cantidad = models.IntegerField(default=0)
    serial = models.CharField(max_length=256)
    descripcion = models.CharField(max_length=256)
    comentario_ingreso = models.CharField(max_length=256)
    comentario_salida = models.CharField(max_length=256)
    fecha_ingreso = models.DateTimeField()
    fecha_salida = models.DateTimeField()
    
