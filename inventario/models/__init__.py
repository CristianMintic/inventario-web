# Aca esta todo lo relacionado con modelos osea lo que me genera la base de datos
# Cada modelo es una tabla
# Se debe importar los modelos para que sean tenidos en cuenta en las migraciones
# Se las clases (ejm: area) de los modelos (ejm: area.py)


from .area import Area                              # el punto significa el lugar de __init__.py
from .inventario import Inventario                  # import relativos
from .productos import Productos            
# from .usuarios import Usuarios
from  inventario.models.usuarios import Usuarios    # otra forma de importarlo , import absoluto

