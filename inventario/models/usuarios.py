from django.db import models



class Usuarios(models.Model):
    id = models.BigAutoField(primary_key=True)
    username = models.CharField(max_length=15, unique=True)
    password = models.CharField(max_length=256)
    name = models.CharField(max_length=30)