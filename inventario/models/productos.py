from django.db import models

class Productos(models.Model):
    id = models.BigAutoField(primary_key=True)
    tipo = models.CharField(max_length=15)
    descripcion = models.CharField(max_length=256)
    